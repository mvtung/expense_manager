<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Symfony\Component\Routing\Exception\RouteNotFoundException;

class UserController extends Controller
{
    public function getUserInfo(Request $request)
    {
        try {
            $user = $request->user();
    
            return response()->json([
                'user' => $user,
            ], 200);
        } catch (RouteNotFoundException $e) {
            return response()->json(['message' => 'Route not found'], 404);
        } catch (\Exception $e) {
            return response()->json(['message' => 'Something went wrong'], 500);
        }
    }
}
