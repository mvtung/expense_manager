<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\ExpenseController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['middleware' => ['api', 'auth:sanctum']], function () {
    Route::middleware('auth:sanctum')->get('/user', [UserController::class, 'getUserInfo']);
    Route::middleware('auth:sanctum')->post('/expense', [ExpenseController::class, 'createExpense']);
    Route::middleware('auth:sanctum')->get('/expense/user/{user_id}', [ExpenseController::class, 'getExpenseByUser']);
    Route::middleware('auth:sanctum')->put('/expense/{expense_id}', [ExpenseController::class, 'updateExpense']);
    Route::middleware('auth:sanctum')->delete('/expense/{expense_id}', [ExpenseController::class, 'deleteExpense']);
});


Route::post('/register', [RegisterController::class, 'register']);
Route::post('/login', [LoginController::class, 'login']);