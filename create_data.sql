CREATE DATABASE expense_manager;

USE expense_manager;

CREATE TABLE users (
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  gender ENUM('Male', 'Female', 'Other') NOT NULL,
  age INT,
  email VARCHAR(255) NOT NULL,
  mobile_number VARCHAR(20),
  address VARCHAR(255),
  password VARCHAR(255) NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP
);

CREATE TABLE expense_categories (
  id INT PRIMARY KEY AUTO_INCREMENT,
  category ENUM('eat_drink', 'shopping', 'health', 'transportation', 'entertainment', 'utilities', 'travel', 'other') NOT NULL
);

CREATE TABLE currency (
  id INT PRIMARY KEY AUTO_INCREMENT,
  currency_code ENUM('VND', 'USD', 'EUR', 'GBP', 'JPY', 'CAD', 'AUD', 'SGD', 'CNY', 'KRW', 'INR', 'MYR', 'THB', 'IDR', 'PHP', 'HKD') NOT NULL
);

CREATE TABLE expense (
  id INT PRIMARY KEY AUTO_INCREMENT,
  name VARCHAR(255) NOT NULL,
  date DATE,
  amount_money DECIMAL(10, 2),
  image VARCHAR(255),
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  deleted_at TIMESTAMP,
  expense_category_id INT,
  user_id INT,
  currency_id INT,
  FOREIGN KEY (expense_category_id) REFERENCES expense_categories(id),
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (currency_id) REFERENCES currency(id)
);

CREATE TABLE budget (
  id INT PRIMARY KEY AUTO_INCREMENT,
  user_id INT,
  category_id INT,
  amount DECIMAL(10, 2),
  start_date DATE,
  end_date DATE,
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (category_id) REFERENCES expense_categories(id)
);

CREATE TABLE expense_share (
  id INT PRIMARY KEY AUTO_INCREMENT,
  expense_id INT,
  shared_with_user_id INT,
  percentage DECIMAL(5, 2),
  amount DECIMAL(10, 2),
  FOREIGN KEY (expense_id) REFERENCES expense(id),
  FOREIGN KEY (shared_with_user_id) REFERENCES users(id)
);

CREATE TABLE expense_reminder (
  id INT PRIMARY KEY AUTO_INCREMENT,
  user_id INT,
  expense_id INT,
  reminder_date DATE,
  message VARCHAR(255),
  FOREIGN KEY (user_id) REFERENCES users(id),
  FOREIGN KEY (expense_id) REFERENCES expense(id)
);

INSERT INTO users (name, gender, age, email, mobile_number, address, password)
VALUES
  ('John Doe', 'Male', 30, 'john.doe@example.com', '1234567890', '123 Street, City', 'password123'),
  ('Jane Smith', 'Female', 25, 'jane.smith@example.com', '9876543210', '456 Avenue, Town', 'password456'),
  ('Mike Johnson', 'Male', 35, 'mike.johnson@example.com', '5555555555', '789 Road, Village', 'password789'),
  ('Lisa Anderson', 'Female', 28, 'lisa.anderson@example.com', '2222222222', '987 Lane, County', 'password987'),
  ('David Lee', 'Male', 40, 'david.lee@example.com', '9999999999', '654 Boulevard, State', 'password654');

INSERT INTO expense_categories (category)
VALUES
  ('eat_drink'),
  ('shopping'),
  ('health'),
  ('transportation'),
  ('entertainment'),
  ('utilities'),
  ('travel'),
  ('other');

INSERT INTO currency (currency_code)
VALUES
  ('VND'),
  ('USD'),
  ('EUR'),
  ('GBP'),
  ('JPY'),
  ('CAD'),
  ('AUD'),
  ('SGD'),
  ('CNY'),
  ('KRW');

INSERT INTO expense (name, date, amount_money, image, expense_category_id, user_id, currency_id)
VALUES
  ('Lunch', '2023-09-10', 15.50, 'lunch.jpg', 1, 1, 1),
  ('Groceries', '2023-09-11', 50.75, 'groceries.jpg', 2, 1, 1),
  ('Doctor Visit', '2023-09-12', 80.00, 'doctor_visit.jpg', 3, 2, 2),
  ('Movie Tickets', '2023-09-13', 25.00, 'movie_tickets.jpg', 5, 3, 1),
  ('Gasoline', '2023-09-14', 40.50, 'gasoline.jpg', 4, 4, 2);

INSERT INTO budget (user_id, category_id, amount, start_date, end_date)
VALUES
  (1, 1, 200, '2023-09-01', '2023-09-30'),
  (1, 2, 150, '2023-09-01', '2023-09-30'),
  (2, 3, 100, '2023-09-01', '2023-09-30'),
  (3, 4, 300, '2023-09-01', '2023-09-30'),
  (4, 5, 50, '2023-09-01', '2023-09-30');

INSERT INTO expense_share (expense_id, shared_with_user_id, percentage, amount)
VALUES
  (1, 2, 50.00, 7.75),
  (2, 1, 25.00, 12.69),
  (3, 1, 75.00, 60.00),
  (4, 3, 33.33, 8.33),
  (5, 2, 10.00, 4.05);

INSERT INTO expense_reminder (user_id, expense_id, reminder_date, message)
VALUES
  (1, 1, '2023-09-15', 'Don''t forget to pay for lunch'),
  (1, 2, '2023-09-20', 'Groceries reminder'),
  (2, 3, '2023-09-25', 'Doctor visit reminder'),
  (3, 4, '2023-09-10', 'Movie tickets reminder');
