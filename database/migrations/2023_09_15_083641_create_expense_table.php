<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('expense', function (Blueprint $table) {
            $table->id();
            $table->string('name', 255)->notNull();
            $table->date('date')->nullable();
            $table->decimal('amount_money', 10, 2)->nullable();
            $table->string('image', 255)->nullable();
            $table->timestamps();
            $table->softDeletes();

            $table->unsignedBigInteger('expense_category_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('currency_id');

            $table->foreign('expense_category_id')->references('id')->on('expense_categories');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('currency_id')->references('id')->on('currency');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('expense');
    }
};
